.PHONY: deps release

deps:
	composer update
	wget https://raw.githubusercontent.com/jedisct1/libsodium.js/master/dist/browsers-sumo/sodium.js -O www/js/lib/sodium.js
	wget https://unpkg.com/browse/openpgp@5.8.0/dist/openpgp.min.js -O www/js/lib/openpgp.min.js

	#wget https://cdn.jsdelivr.net/npm/jszip/dist/jszip.min.js -O www/js/lib/jszip.min.js
	#wget https://cdn.jsdelivr.net/npm/officeprops/src/officeprops.js -O www/js/lib/officeprops.js

release:
	zip dist/potimail-compat.zip config lib vendor www
	zip dist/potimail.zip config lib www
	cd client && make phar && make phar-nocompat
