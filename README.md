# Potimail — a secure, encrypted, minimalist e-mail service

This is a lightweight alternative to ProtonMail, Tutanota, Mailfence, and others for self-hosting your own encrypted e-mail service. Except, this is much better (yes!).

* More privacy-friendly
* More ecological (less data!)
* Offline use
* 100% open source! Self-host it on your own server!
* Only requires PHP 7.4+, nothing else

## Features

* Lightweight
* Mobile-friendly
* Simple interface, easy to use
* Designed for lightweight mailboxes (climate-friendly + can run thousands of users on a small VPS)
* PHP + libsodium + OpenPGP.js
* No database required, only flat files are used
* Markdown support
* Multiple languages support
* Multi-domain support
* Messages and files are stored compressed on the server, to reduce required disk space

### Security features

* PGP/GPG built-in encryption, client-side
* Nothing is stored in clear text:
	* Incoming e-mails are encrypted using the users' public key when received
	* Archived messages are encrypted by the user
* Messages are retrieved in the web browser and immediately deleted from the server (unless archived)
* No HTML support, HTML emails are transformed to plain text
* The password is never sent to the server
* Messages not coming from your contacts are not displayed in your inbox
* Messages between users of a same server don't transit using SMTP, but are directly stored in the recipients queue, meaning there is zero metadata or log
* Recovery passphrase, no "forgotten password" feature
* Login without sending the password, using SRP + libsodium
* Send encrypted e-mails to people not using PGP
* Automatic deletion of messages, per folder
* Web Key Directory for users, to enable automatic encryption from other providers
* Use other servers Web Key Directory to discover contacts keys

### Privacy

* You can require users to use Tor to login
* The user name, e-mail address, and domain, are not stored
* No logs
* Minimal metadata
* On encrypted e-mails, the subject is encrypted in the message, the subject is empty
* On encrypted e-mails, the sending date is random
* Messages files dates are changed to a random value (+/-7 days), so that it's not possible to find out when exactlt a message arrived or was sent
* Attached images are resized in browser, all metadata is removed
* Warning if sending a file that can leak metadata
* Warning if opening a file that can be contain malicious code (anything else than images basically)

### Attachments

* Large attachments are automatically deleted from incoming un-encrypted messages
* Only a restricted set of files types is allowed, forbidden attachments are deleted
* Attachments are stored separately:
	* You can browse all messages attachments without opening messages
	* You can delete an attachment from a message at any time

### Better email UX

* 

### Climate friendly

## Limitations

* No folders, only Inbox, Sent and Drafts (currently)
* No filtering (currently)
* No spam filtering
* No HTML
* No embedded images

## Clients

* poti, a command-line client
* potiweb, the default javascript web client

## Differences with other solutions

| | Potimail | ProtonMail | TutaNota | Common e-mail providers |
| --: | -- | -- | -- | -- |
| PGP support | Yes | Yes | No | No |
| Easy to use | Yes | Average | Average | Average |
| Open source | Server + Client | Client only | Client only | No |
| Offline browser client | Yes | ? | ? | No |
| Expiring messages | Yes | Yes | No | No |
| Encrypted messages to external contacts | Yes | Yes | Yes | No |
| Secure defaults | Yes | No | No | No |
| Calendar | No | Yes | Yes | Maybe |
| IMAP/SMTP access | No | Paid accounts only | No | Yes |


## Potimail.com service

This is a demo of Potimail.

* Free
* 1MiB of storage for archived e-mails
* 50MiB for queued e-mails
* Accounts are deleted after 6 months without a login
* Sending to external e-mail addresses is limited.
* You can upgrade to 200 MiB + 500 MiB by sending a donation :-)

## How does it work?

1. When an e-mail is received by the local SMTP server, it forwards it to Potimail. The message is then parsed.
2. If the e-mail is not encrypted, large attachments and forbidden attachments are removed, the attachments are extracted, and the message and attachments are encrypted using the users public key. The message is then stored in the users queue.
3. If the e-mail is encrypted, we also encrypt it using the users public key, so that the headers are encrypted and no metadata is left unencrypted. The message is then stored in the users queue.
4. When the user refreshes their inbox, messages in the queue are retrieved by the browser and decrypted (in the browser).
5. If a message is encrypted by the sender, it is then decrypted. The messages are then processed to filter attachments and transform HTML to plain text. Attachments are extracted and stored separately.
6. The messages are encrypted and stored in the users browser data store. The list of messages and the search index are also encrypted. This means there should be no unencrypted data left on your computer if you haven't provided your password.

By default, messages are only stored in the users browser (secure by default). Users can choose to archive important messages on the server, so that they will be accessible from other browsers.

## How secure is this?

* 100% open-source
* We are only using widely-used and recognized open source libraries for encryption:
  * libsodium to encrypt users data on the server (or [Sodium_Compat](https://github.com/paragonie/sodium_compat) if you don't have libsodium installed)
  * [libsodium.js](https://github.com/jedisct1/libsodium.js) to encrypt/decrypt data between server and browser
  * [OpenPGP.js](https://openpgpjs.org) to encrypt/decrypt messages in the browser
* This software didn't have any security audit (yet)
* There still can be bugs
* The server you are using might tamper with the code, and retrieve your password or your private key, and decrypt what you have on the server: use a server that you trust!

### Secure Remote Password login

* Used by 1password
* Prevents sending the password to the server
* https://github.com/simbo1905/thinbus-php-srp-demo
	* https://bitbucket.org/simon_massey/thinbus-php/src/master/
	* https://bitbucket.org/simon_massey/thinbus-srp-js/src/master/
* https://eric.mann.blog/a-libsodium-based-secure-remote-password-scheme-for-php/
* https://code.falk-m.de/srp/
	* https://github.com/falkmueller/srp/blob/master/php/lib/srp.php
* https://github.com/shlima/srp6a-webcrypto
	* https://cdn.jsdelivr.net/npm/@wault-pw/srp6a-webcrypto@1.0.9/dist/
	* used with https://github.com/shlima/srp6ago

* Très simple
* Max 10 Mo + 50 Mo de mails en attente
* No logs
* Minimum metadata
* Pas de messages envoyés
* Drafts côté client
* Stockage des users : SQLite
* Stockage des mails :
	- un répertoire par personne
	- un sous-rép inbox
	- un sous-rép queue
* Pas de dossiers
* Pas de filtres
* Possible d'envoyer des mails en local, en direct (sans passer par mailer)
* Réception de mails via un script + EXIM
* OpenPGP.js côté client
* Interopérable avec tout client mail utilisant PGP
* Stockage chiffré côté serveur (avec la clé publique du compte)
* Création de compte: adresse/mot de passe + phrase de récupération
	https://crypto.stackexchange.com/questions/86632/how-are-recovery-keys-possible-if-something-is-encrypted-using-a-password
* Le mot de passe n'est pas transmis au serveur, à la place un hash random est généré et chiffré par le serveur, et déchiffré côté client, avec sa clé privée (côté client)
* Stockage de la clé publique sur le serveur, et de la clé privée, chiffrée avec la passphrase

* Les mails reçus :
	* si contenu chiffré : on garde, on chiffre avec la clé PGP publique du compte
	* si contenu non chiffré : on supprime les pièces jointes dangereuses/grosses, on supprime le HTML, on chiffre
* Quand la personne se connecte : on récupère les mails reçus :
	- si chiffré PGP: on déchiffre, on filtre le HTML et les pièces jointes
		-> si conservé sur serveur : on remplace le mail sur serveur par une version re-chiffrée par clé user
	- sinon : on récupère et déchiffre

* Stockage automatique de la première clé publique PGP transmise par un contact
* Chiffrement automatique des messages
* Génération automatique des clés PGP à la création de compte
* Pas de HTML, mais du Markdown
* UX claire de si c'est chiffré ou pas
* Brouillons non-synchronisés

== Plus tard ==

* Expiration automatique de messages envoyés
* Suppression automatique de messages par dossier après un certain délai
* Envoi de messages chiffrés à des gens qui n'ont pas PGP, en utilisant PrivateBin
* Possible d'envoyer des mails à l'extérieur si compte payant + validation
