<?php declare(strict_types=1);

// create phar
$phar = new \Phar($argv[1]);

$phar->setSignatureAlgorithm(\Phar::SHA1);

// start buffering, mandatory to modify stub
$phar->startBuffering();

if ($argv[2] == '--with-sodium-compat') {
	$phar->buildFromIterator(
		new RecursiveIteratorIterator(
			new RecursiveDirectoryIterator(__DIR__ . '/../vendor')
		),
		__DIR__
	);
}

$phar->addFile(__DIR__ . '/client.php', 'client.php');

$stub = $phar->createDefaultStub(__DIR__ . '/stub.php');
$stub = "#!/usr/bin/env php\n" . $stub;

$phar->setStub($stub);

$phar->stopBuffering();

// compress to gzip
$phar->compress(Phar::GZ);
