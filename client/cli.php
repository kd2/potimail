<?php declare(strict_types=1);

namespace Poticli;

require __DIR__ . '/../lib/KD2/HTTP.php';
require __DIR__ . '/client.php';
$cli = new CLI;
$cli->run($_SERVER['argv']);

use \stdClass;

class CLI
{
	protected string $server;
	protected string $address;
	protected string $password;

	public function run(array $argv): void
	{
		$options = [];
		$parameters = [];
		$parse_options = true;

		$raw = $_SERVER['argv'];
		array_shift($raw);

		while (null !== ($a = array_shift($raw))) {
			if ($parse_options) {
				if ($a === '--') {
					$parse_options = false;
					continue;
				}
				elseif (substr($a, 0, 2) == '--') {
					$options[substr($a, 2)] = array_shift($raw);
					continue;
				}
				elseif (substr($a, 0, 1) == '-') {
					$options[substr($a, 1)] = substr($a, 2);
					continue;
				}
			}

			$parameters[] = $a;
		}

		$command = array_shift($parameters);
		$options = (object) $options;

		if (!method_exists($this, 'cmd_' . $command)) {
			printf("Unknown command: %s\n", $command);
			exit(1);
		}

		$this->client = new Client;

		$this->loadConfig();

		$this->{'cmd_' . $command}($options, $parameters);
	}

	protected function loadConfig(): void
	{
		return;
		if (!file_exists($this->config_path)) {
			return;
		}

		foreach (parse_ini_file($this->config_file) as $key => $value) {
			if (property_exists($this, $key)) {
				$this->$key = $value;
			}
		}
	}

	public function fail(string $msg, ...$args): void
	{
		printf($msg . "\n", ...$args);
		exit(1);
	}

	public function readPassword(string $prompt): string
	{
		echo $prompt;
		system('stty -echo');
		$password = trim(fgets(STDIN));
		system('stty echo');
		echo "\n";
		return $password;
	}

	public function readInput(string $prompt): string
	{
		return readline($prompt);
	}

	public function cmd_register(stdClass $options)
	{
		$domain = $options->domain ?? $this->readInput('Domain name: ');
		$name = $options->name ?? $this->readInput('User name: ');

		if (isset($options->password)) {
			$pw1 = $options->password;
		}
		else {
			$pw1 = $this->readPassword('Login password: ');
			$pw1b = $this->readPassword('Repeat password: ');

			if ($pw1 !== $pw1b) {
				return $this->fail("Password doesn't match.");
			}
		}

		if (isset($options->panic)) {
			$pw2 = $options->panic;
		}
		else {
			$pw2 = $this->readPassword('Panic password: ');
			$pw2b = $this->readPassword('Repeat password: ');

			if ($pw2 !== $pw2b) {
				return $this->fail("Password doesn't match.");
			}
		}

		$words = file('/usr/share/dict/words');
		$pass = [];

		while (count($pass) < 12) {
			$word = strtolower(trim($words[random_int(0, count($words) - 1)]));

			if (ctype_alnum($word)) {
				$pass[] = $word;
			}
		}

		$pass = implode(' ', $pass);

		printf("Your recovery passphrase is:\n%s\n\nPlease write it down somewhere.\nThis is only way to recover access to your account if you forget your password.\n", wordwrap($pass));

		$this->client->register($name, $domain, [
			'login'   => $pw1,
			'wipe'    => $pw2,
			'recover' => $pass,
		]);
	}

	public function cmd_login(stdClass $options): void
	{
		$address = $options->address ?? $this->readInput('Address: ');
		$password = $options->password ?? $this->readPassword('Password: ');

		$this->client->login($address, $password, 'login');
	}

	public function cmd_configure(): void
	{

	}

}
