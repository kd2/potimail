<?php declare(strict_types=1);

namespace Poticli;

use KD2\HTTP;

class Client
{
	protected string $gpg_home;
	protected string $server;

	public function __destruct()
	{
		if (isset($this->gpg_home)) {
			@system(sprintf('rm -rf %s', escapeshellarg($this->gpg_home)));
		}
	}

	public function gpgCleanArmor(string $str): string
	{
		$str = preg_replace('/-----(?:END|BEGIN) PGP[^-]+-----/', '', $str);
		return str_replace("\n", '', trim($str));
	}

	public function gpgInit(): void
	{
		if (!isset($this->gpg_home)) {
			$this->gpg_home = trim(shell_exec('mktemp -d'));
		}
	}

	public function gpgSetPassphrase(string $pw)
	{
		$this->gpgInit();

		$pwtmp = $this->gpg_home . '/_pass';
		file_put_contents($pwtmp, $pw);;
	}

	public function gpg(string $args, ...$params)
	{
		$this->gpgInit();

		$params = array_map('escapeshellarg', $params);

		$cmd = sprintf('export GNUPGHOME=%s; gpg --passphrase-file %s --pinentry-mode loopback %s 2>/dev/null',
			escapeshellarg($this->gpg_home),
			escapeshellarg($this->gpg_home . '/_pass'),
			sprintf($args, ...$params)
		);

		return shell_exec($cmd);
	}

	public function request(string $method, string $uri, ?array $parameters = null)
	{
		$url = rtrim($this->server, '/') . '/'. $uri;
		$http = new HTTP;

		if ($method == 'POST') {
			$r = $http->POST($url, $parameters);
		}
		elseif ($method == 'GET') {
			$r = $http->GET($url);
		}
		else {
			$data = null;
			$headers = null;

			if ($method == 'PUT') {
				$data = file_get_contents($parameters['file']);
			}
			elseif ($method == 'MOVE') {
				$headers['Destination'] = $parameters['destination'];
			}

			$r = $http->request($method, $url, $data, $headers);
		}

		if ($r->fail) {
			throw new \RuntimeException('Error in HTTP request');
		}
		elseif ($r->status > 299) {
			throw new \RuntimeException(sprintf('Server error: %d = %s', $r->status, $r->body));
		}

		return json_decode($r->body);
	}

	public function passwordHash(string $password, ?string &$salt): string
	{
		$salt ??= random_bytes(SODIUM_CRYPTO_PWHASH_SALTBYTES);
		$r = sodium_crypto_pwhash(
			SODIUM_CRYPTO_KX_SEEDBYTES,              // Desired hash output length
			$password,                               // User's known password
			$salt,                                   // Random salt
			SODIUM_CRYPTO_PWHASH_OPSLIMIT_INTERACTIVE, // Password hashing ops limit
			SODIUM_CRYPTO_PWHASH_MEMLIMIT_INTERACTIVE  // Password hashing memory limit
		);

		sodium_memzero($password);
		return $r;
	}

	public function passwordKeys(string $password, ?string &$salt): array
	{
		$keypair = $this->passwordKeypair($password, $salt);

		return [
			sodium_crypto_kx_publickey($keypair),
			sodium_crypto_kx_publickey($keypair),
		];
	}

	public function passwordKeypair(string $password, ?string &$salt): string
	{
		$keypair = sodium_crypto_kx_seed_keypair($this->passwordHash($password, $salt));
		var_dump("KP ($password): " . sodium_bin2hex($keypair));
		return $keypair;
	}

	public function register(string $name, string $domain, array $passwords): bool
	{
		$keys = [];
		$types = ['login', 'recover', 'wipe'];

		$name = trim(strtolower($name));
		$domain = trim(strtolower($domain));

		if (count(array_unique($passwords)) !== count($passwords)) {
			throw new \InvalidArgumentException('Some passwords have the same value');
		}

    	$user_keypair = sodium_crypto_box_keypair();
    	$user_public_key = sodium_crypto_box_publickey($user_keypair);
    	$user_private_key = sodium_crypto_box_secretkey($user_keypair);

		foreach ($passwords as $type => $password) {
			if (!in_array($type, $types, true)) {
				throw new \InvalidArgumentException('Invalid password type: ' . $type);
			}

			// Prefix the password
			$password = $type . ':' . $password;

			$salt = null;
			list($identifier, $private_key) = $this->passwordKeys($password, $salt);
			sodium_memzero($password);

			$nonce = random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);
			$encrypted_private_key = sodium_crypto_secretbox($user_private_key, $nonce, $private_key);
			$encrypted_private_key = sodium_bin2hex($nonce) . ':' . sodium_bin2hex($encrypted_private_key);
			sodium_memzero($private_key);

			$keys[] = [
				'identifier' => sodium_bin2hex($identifier),
				'salt'       => sodium_bin2hex($salt),
				'private'    => $encrypted_private_key,
				'type' => $type,
			];
		}

		$this->gpgSetPassphrase(sodium_bin2hex($user_private_key));
		$this->gpg('--quick-gen-key %s', $name . '@' . $domain);
		$pgp_private_key = $this->gpg('--armor --export-secret-keys');
		$pgp_public_key = $this->gpg('--armor --export');

		$nonce = random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);
		$encrypted_pgp_private_key = sodium_crypto_secretbox($pgp_private_key, $nonce, $user_private_key);
		$encrypted_pgp_private_key = sodium_bin2hex($nonce) . ':' . sodium_bin2base64($encrypted_pgp_private_key, SODIUM_BASE64_VARIANT_ORIGINAL);

		sodium_memzero($user_private_key);
		sodium_memzero($pgp_private_key);

		$data = compact('name', 'domain', 'keys') + [
			'public_key'      => sodium_bin2hex($user_public_key),
			'pgp_public_key'  => $pgp_public_key,
			'pgp_private_key' => $encrypted_pgp_private_key,
		];

		var_dump($data['keys']);

		$this->server = $domain;
		$this->request('POST', 'register', $data);
		return true;
	}

	public function login(string $address, string $password, string $mode)
	{
		$address = trim(strtolower($address));

		$pos = strrpos($address, '@');

		if (!$pos) {
			return null;
		}

		$this->server = substr($address, $pos+1);

		$r = $this->request('POST', 'auth/init', compact('address'));

		if (!$r || !is_array($r) || !count($r)) {
			return null;
		}

		foreach ($r as $i) {
			$salt = sodium_hex2bin($i->salt);
			$keypair = $this->passwordKeypair($mode . ':' . $password, $salt);

			// Derive the shared secret
			$server_key = sodium_hex2bin($i->server_public_key);
			$session_keys = sodium_crypto_kx_client_session_keys($keypair, $server_key);
			$client_secret = $session_keys[0];
			$server_secret = $session_keys[1];

			// Validate the server's proof
			$server_proof = sodium_hex2bin($i->server_proof);
			$message = substr($server_proof, 0, 32);
			$hash = substr($server_proof, 32);

			if (!hash_equals(sodium_crypto_generichash($message, $server_secret), $hash)) {
				printf("Skip salt=%s\n", $i->salt);
				continue;
			}

			// Generate the client's proof
			$client_message = random_bytes(32);
			$client_hash = sodium_crypto_generichash($client_message, $client_secret);
			$proof = sodium_bin2hex($client_message . $client_hash);

			// POST this array to the server to authenticate
			$data = [
				'address'           => $address,
				'server_public_key' => $i->server_public_key,
				'client_proof'      => $proof,
			];

			var_dump('message1: ' . sodium_bin2hex($client_message), sodium_bin2hex($client_hash));

			$r = $this->request('POST', 'auth/verify', $data);

			if (!$r || !is_object($r)) {
				return null;
			}

			var_dump($r);
		}

		return null;
	}
}
