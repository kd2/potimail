<?php declare(strict_types=1);

$sodium_path = __DIR__ . '/vendor/paragonie/sodium_compat/autoload.php';

// Fallback for libsodium
if (!extension_loaded('sodium')) {
	if (file_exists($sodium_path)) {
		require_once $sodium_path;
	}
	else {
		die("Error: Potimail requires libsodium, and it's not installed\n");
	}
}

require __DIR__ . '/client.php';
