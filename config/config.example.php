<?php

namespace Potimail;

/**
 * @var string
 */
const SERVICE_NAME = 'My Potimail service';

/**
 * Users storage path
 * @var string
 */
const STORAGE_PATH = __DIR__ . '/../data';

/**
 * Cache storage path
 * @var string
 */
const CACHE_DIR = __DIR__ . '/../cache';

/**
 * List of valid domains (FQDNs) that will be proposed during subscription.
 * This is the "domain" part of the address the user will get.
 *
 * @var array
 */
const DOMAINS = [
	'potimail.localhost',
];

/**
 * User registering mode.
 *
 * This can be:
 * - open: anyone can register
 * - closed: nobody can register
 * - invite: people can only register using an invitation code
 */
const REGISTER_MODE = 'open';

/**
 * Default user settings
 * This is the default settings used for new users.
 *
 * You can change these for each user individually.
 * For example, if you want to only allow paying users
 * to send external e-mails.
 */
UserProfile::setDefaults([
	/**
	 * Whether to allow sending e-mails outside of Potimail servers (SMTP).
	 *
	 * @var bool
	 */
	'allow_email_sending' => true,

	/**
	 * Whether to allow sending messages to other Potimail servers.
	 *
	 * @var bool
	 */
	'allow_network_sending' => true,

	/**
	 * Maximum number of messages sent for each 24h period
	 *
	 * @var int
	 */
	'max_sent_messages_24h' => 100,

	/**
	 * Quota for mailbox, not including queued messages
	 *
	 * @var int in bytes
	 */
	'quota' => 10 * 1024 * 1024, // 10 MiB

	/**
	 * Quota for queued messages
	 *
	 * @var int in bytes
	 */
	'queue_quota' => 50 * 1024 * 1024, // 50 MiB

	/**
	 * Maximum message size
	 *
	 * @var int in bytes
	 */
	'max_message_size' => 15*1024*1024, // 15 MiB

	/**
	 * Maximum attachment size for incoming e-mails
	 * Larger attachments will be deleted
	 * Set to zero to disable the limit.
	 *
	 * @var int in bytes
	 */
	'max_attachment_size' => 10*1024*1024, // 10 MiB

	/**
	 * Number of invitation codes a user can generate.
	 *
	 * @var int
	 */
	'invitation_count' => 0,

	'auto_delete' => [
		Mailbox::QUEUE => 180,
		Mailbox::INBOX => 365,
		Mailbox::SENT => 365,
		Mailbox::DRAFTS => 30,
	],
]);

/**
 * Enable or disable onion routing of messages
 * between Potimail servers.
 *
 * Note that onion routing is disabled if less than 3 servers
 * are in the local server database. This means that your server
 * needs to send messages to 3 other servers directly before being
 * able to use onion routing.
 *
 * Only messages between Potimail users use Onion routing, regular e-mails
 * are never relayed. This is to avoid spam.
 */
const ENABLE_ONION_ROUTING = true;

/**
 * List of allowed attachment types
 *
 * The filtering will use both the file extension, and the MIME type.
 *
 * For encrypted e-mails, the web client will remove these.
 * For unencrypted messages, the attachments will be removed.
 */
const ALLOWED_ATTACHMENT_TYPES = [
	'asc'  => 'application/pgp-keys',
	'key'  => 'application/pgp-signature',
	'pgp'  => 'application/pgp-encrypted',
	'png'  => 'image/png',
	'jpeg' => 'image/jpeg',
	'jpg'  => 'image/jpeg',
	'gif'  => 'image/gif',
	'webp' => 'image/webp',
	'md'   => 'text/plain',
	'txt'  => 'text/plain',
	'csv'  => 'text/csv',
	'pdf'  => 'application/pdf',
	'ods'  => 'application/vnd.oasis.opendocument.spreadsheet',
	'odt'  => 'application/vnd.oasis.opendocument.text',
	'odp'  => 'application/vnd.oasis.opendocument.presentation',
	'doc'  => 'application/msword',
	'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
	'xls'  => 'application/vnd.ms-excel',
	'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
	'ppt'  => 'application/vnd.ms-powerpoint',
	'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
	'mp3'  => 'audio/mpeg',
	'ogg'  => 'audio/ogg',
	'opus' => 'audio/ogg',
	'ics'  => 'text/calendar',
];

/**
 * URL where list of Tor exit nodes IPs can be downloaded
 * Set to NULL to disable the login/register message "You are not using Tor, your identity might be revealed"
 *
 * @var string|null
 */
const TOR_EXIT_NODES_LIST_URL = 'https://check.torproject.org/torbulkexitlist';

/**
 * Whether to show PHP errors or not (development mode)
 * @var bool
 */
const ERRORS_SHOW = true;

/**
 * Path to error log file
 * @var string|null
 */
const ERRORS_LOG = __DIR__ . '/../error.log';

/**
 * E-mail address where PHP errors should be reported
 * @var string|null
 */
const ERRORS_EMAIL = null;
