# How does it work?

## Overview

1. When an e-mail is received by the SMTP server, it forwards it to a Potimail shell script. The message is then parsed.
2. If the e-mail is not encrypted, large attachments and forbidden attachments are removed, and the e-mail is then encrypted using the users public key. The message is then stored in the users queue.
3. If the e-mail is encrypted, we also encrypt it using the users public key, so that the headers are encrypted and no traces can be found. The message is then stored in the users queue.
4. When the user refreshes their inbox, messages in the queue are retrieved by the browser, deleted from the server and decrypted (in the browser).
5. If a message is encrypted by the sender, it is then decrypted. The messages are then processed to filter attachments and transform HTML to plain text.
6. The messages are then encrypted and stored in the users browser IndexedDB. The list of messages and the search index are also encrypted. This means there should be no unencrypted data left on your computer if you haven't provided your password.

By default, messages are only stored in the users browser (secure by default). Users can choose to archive important messages on the server, so that they will be accessible from other browsers.

## Registration

When you register, Potimail generates:

* a public and private key (called the "internal keys" below), using libsodium, used for encryption between the server and your browser
* a derived key (verifier) from the password, and a random salt (this is used to login without sending your password to the server)
* a public and private PGP keys, this is used for decrypting messages coming from other people, and encrypting your outgoing messages

The PGP private key is then encrypted using your internal private key.

Then this internal private key is encrypted using your password (hashed using Argon2id, to make it harder to brute-force your password).

The internal private key is also encrypted using a recovery passphrase (12 words), generated randomly. This will be used to recover your private key if you lose or forget your password. This "encrypted recovery key" is used to derive a key, called a verifier, using a random salt.

At the end, the browser will store:

* internal public key
* encrypted internal private key
* PGP public key
* encrypted PGP private key

And will send to the server:

* the users e-mail address (obviously)
* the internal public key
* the PGP public key
* the encrypted internal private key
* the private key verifier
* the private key salt
* the encrypted recovery key
* the recovery key verifier
* the recovery key salt
* the encrypted PGP private key

As you can see, your password is never sent to the server.

## Login

When you login, the Secure Remote Password (SRP) protocol is used:

1. Browser initiates the authentication session by submitting their e-mail address to the server
2. The server creates an ephemeral (random and temporary) public/private keypair.
3. The server uses its private key along with the browser's public key to derive a shared secret key.
4. The server creates a cryptographic proof of its shared secret—a hash of the derived shared secret.
5. The server then sends the users' original salt, its own public key, and its proof of the derived shared key back to the browser.
6. The browser uses the salt with its known password to re-derive its public/private keypair.
7. The browser uses its private key with the server’s public key to derive the same shared secret.
8. The browser validates the server’s proof of the shared secret and, if valid, creates its own proof and sends it to the server
9. The server independently verifies the browser's proof and, if valid, continues with an active user authentication session for the client.

## Recovery

For recovery, the same process as the login is used, except that we are using the passphrase instead of the password.
