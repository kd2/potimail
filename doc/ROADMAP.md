# ROADMAP

## 1.0 - Working server + CLI client

* Receive unencrypted or PGP-encrypted e-mails
* Store them encrypted for the user
* Working API:
	* register
	* login
	* logout
	* fetch messages
	* store messages
	* delete messages
	* move messages
	* store custom-files (eg. contacts)
* Automatic expiration of sent messages
* Automatic deletion of messages, per folder
* Private key recovery using recovery passphrase (BIP-39?)
	* https://medium.com/mycrypto/the-journey-from-mnemonic-phrase-to-address-6c5e86e11e14
	* https://www.halborn.com/blog/post/what-is-a-bip39
	* https://github.com/furqansiddiqui/bip39-mnemonic-php/blob/master/src/BIP39.php
	* https://github.com/tarun1475/Nodejs-implementation-of-BIP39-for-multicoin-crypto-wallet
* Web Key Directory to discover contacts keys: https://wiki.gnupg.org/WKD
	* see https://sektioneins.de/en/blog/18-11-23-gnupg-wkd.html
	* Z-Base32: https://github.com/openpgpjs/wkd-client/blob/master/src/wkd.js
	* https://netwerk.io/setting-up-gpg-web-key-directory-wkd/
* Attach the PGP pubkey to outgoing messages to first-time recipients
* Duress password:
	* Have a second private key and identifier, for a second password.
	* If this password is used, the client will wipe all local data from browser, AND will send a /wipe request to the server.
	* The PGP public key will stay the same, but all the other keys will be overwritten with random data.
	* The "emergency wipe key" will replace the normal private key (you will only be able to use your duress password to login, the old password will stop working).
	* This means that to an outsider, the user will not have changed (the public key will stay the same), but won't be able to decrypt any received messages. The mailbox will appear as if it was empty.

## 2.0 - Working javascript web client

* Use no external JS libraries
* Forbid from using compromised password (using HIBP API from JS client: https://haveibeenpwned.com/API/v3#PwnedPasswords)
* Remove metadata from Office files, using https://github.com/TorkelV/officeprops
* Password strength meter
* Search, using https://github.com/nextapps-de/flexsearch for example
* Contacts (stored encrypted on server)
* Offline mode
* Sign the JS client code with developers key, publish it for each version, and have the JS client check the signature when an update shows up
* Have the CLI client able to verify the JS client code of a Potimail instance

## 3.0 - Private mails

* Use Sodium to encrypt e-mails between users of different servers
* Ability to send messages to other Potimail instances using an HTTPS API

## Future

* Optional onion routing of messages between Potimail instances
* Import keys from Autocrypt header: https://autocrypt.org/level1.html#the-autocrypt-header
* Encrypted reply to an ephemeral web message
* Panic button: wipe all local data, logout
* Support for POP3/IMAP fetch and SMTP send, so that you can use this with an existing email account
* CLI mailing-list tool (like https://schleuder.org): will retrieve messages, decrypt, and resend, encrypted, to recipients
* Local SMTP/IMAP gateway client
