## ProtonMail issues

Shortcomings addressed in the paper [An Analysis of the ProtonMail Cryptographic Architecture](https://eprint.iacr.org/2018/1121.pdf) have been taken into account:

1. On not being able to authenticate the code served by the webmail:
	* There are no solutions to this, as Javascript code cannot be signed currently in browsers
	* However, the Potimail developers provide a PGP signature of the webmail code.
	* This signature can be used to verify the source code of the webmail served by a server, using our CLI client.
	* The CLI client can also check this using a valid user session, meaning it would detect any tampering with the webmail code even if the code is changed only when a valid user logs in.
	* This will not detect changes to the code that only affect a specific user though.
	* The webmail is also able to fetch and verify the code signature.
2. On the storage of private keys on the webserver:
	* This is done by default, because it is just more convenient for most people.
	* But you can make the choice to not store the private PGP key on the server. This means you must save the private key on your device.
3. On "Encrypt-to-Outside":
	* Yes, there are risks, but we believe this is better than clear-text e-mails
4. On weak passwords:
	* Potimail webmail is using HIBP API to deny using compromised passwords
	* A password meter is also used to make sure the password is strong
	* It is suggested to use a passphrase instead of a password
5. On weak user authentication:
	* We use Argon2id, making dictionary attacks much harder


## Duress password

See also these Android apps, they can wipe your phone, but it will be more obvious:

* https://guardianproject.info/apps/info.guardianproject.ripple/
* https://github.com/x13a/Wasted
* https://github.com/x13a/Duress