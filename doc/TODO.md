TODO

* bcrypt for verifier hashing on client-side for SRP, https://github.com/dcodeIO/bcrypt.js/blob/master/README.md
* recovery passphrase:
	* encrypt private key using verifier
	* provide server-side rate limiting (very agressive)
	* captcha
* add login attempts rate-limiting
* add captcha for registration

Crypto best practices:
https://gist.github.com/atoponce/07d8d4c833873be2f68c34f9afc5a78a?permalink_comment_id=2768614