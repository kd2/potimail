<?php

class Directory
{
	const CACHE_PATH = STORAGE_PATH . '/cache/directory/%.2s/%s';

	/**
	 * Decode given a z-base32 string back to data
	 */
	static public function decodeZBase32(string $data): string
	{
		if (empty($data)) {
			return '';
		}

		static $chars = 'ybndrfg8ejkmcpqxot1uwisza345h769';

		$data = str_split($data);
		$data = array_map(function ($character) use ($characters) {
			$index = strpos($characters, $character);
			return sprintf('%05b', $index);
		}, $data);

		$binary = implode('', $data);

		/* Split to eight bit chunks. */
		$data = str_split($binary, 8);

		/* Make sure binary is divisible by eight by ignoring the incomplete byte. */
		$last = array_pop($data);
		if ((null !== $last) && (8 === strlen($last))) {
			$data[] = $last;
		}

		return implode('', array_map(function ($byte) {
			return chr((int)bindec($byte));
		}, $data));
	}

	static public function get(string $hash, string $domain): ?string
	{
		$hash = Directory::decodeZBase32($hash);
		$hash = sodium_bin2hex($hash);

		$user = User::get(User::getHashFromWKD($hash, $domain));

		if (!$user) {
			return null;
		}

		return base64_decode($user->profile()->pgp_public_key);
	}

	static public function find(string $hash, string $server): ?array
	{
		if (!filter_var($server, FILTER_VALIDATE_DOMAIN)) {
			throw new \InvalidArgumentException('Invalid server: ' . $server);
		}

		$hash = User::getHashFromWKD($hash, $server);
		$user = new User($hash);

		// Find if this profile already exists
		if ($user->exists()) {
			$profile = $user->profile();
			return ['public_key' => $profile->public_key, 'pgp_public_key' => $profile->pgp_public_key];
		}

		$path = sprintf(self::CACHE_PATH, $hash);

		// Find if this profile is already cached
		if (file_exists($path) && filemtime($path) > time() - 3600*24*30) {
			$key = file_get_contents($path);

			if (!trim($key)) {
				return null;
			}

			return ['pgp_public_key' => gzinflate($key)];
		}

		$dns = $server;

		if (function_exists('idn_to_ascii')) {
			$dns = idn_to_ascii($dns);
		}

		if (!checkdnsrr($server, 'A')) {
			return null;
		}

		$r = (new HTTP)->GET('https://' . $server . '/.well-known/openpgpkey/hu/' . $hash);

		if ($r->code != 200 || empty($r->body)) {
			file_put_contents($path, '');
			return null;
		}

		$key = base64_encode($r->body);

		file_put_contents($path, gzdeflate($key));

		return ['pgp_public_key' => $key];
	}
}
