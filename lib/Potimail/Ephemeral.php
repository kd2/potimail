<?php

namespace Potimail;

class Ephemeral
{
	const OK = 1;
	const BURN = 2;
	const NOT_FOUND = -1;
	const INVALID = -2;

	const PATH = STORAGE_PATH . '/ephemerals';

	static public function garbageCollect(): void
	{
		if (!file_exists(self::PATH)) {
			return;
		}

		foreach (glob(self::PATH . '/*') as $file) {
			if (basename($file)[0] === '.') {
				continue;
			}

			$time = filemtime($file);

			if ($time !== 0 && $time < time()) {
				@unlink($file);
			}
		}
	}

	static public function create(string $private_key, string $message, int $expiry = 86400): string
	{
		$id = hash('sha256', random_bytes(16));
		$path = self::PATH . '/' . $id;

		@mkdir(dirname($path), 0777, true);
		file_put_contents($path, json_encode(compact('private_key', 'message')));

		if ($expiry === 0) {
			touch($path, 0);
		}
		else {
			touch($path, time() + $expiry);
		}

		return $id;
	}

	static public function status(string $id): int
	{
		if (!ctype_alnum($id) || strlen($id) != 64) {
			return self::INVALID;
		}

		$path = self::PATH . '/' . $id;

		if (!file_exists($path)) {
			return self::NOT_FOUND;
		}

		$time = filemtime($path);

		if ($time === 0) {
			return self::BURN;
		}

		if ($time < time()) {
			@unlink($path);
			return self::NOT_FOUND;
		}

		return self::OK;
	}

	static public function get(string $id): ?array
	{
		$status = self::status($id);

		if (!in_array($status, [self::OK, self::BURN], true)) {
			return null;
		}

		$path = self::PATH . '/' . $id;
		$out = json_decode(@file_get_contents($path), true);

		if ($status === self::BURN) {
			@unlink($path);
		}

		return $out;
	}
}
