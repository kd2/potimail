<?php

namespace Potimail;

class Mailbox
{
	const INBOX = 'inbox';
	const QUEUE = 'queue';
	const SENT = 'sent';
	const DRAFTS = 'drafts';
	const FOLDERS = [self::INBOX, self::QUEUE, self::SENT, self::DRAFTS];

	protected User $user;

	public function __construct(User $user)
	{
		$this->user = $user;
		$this->key = sodium_hex2bin($user->profile()->public_key);
	}

	public function path(string $folder, ?string $msg = null): string
	{
		if (!in_array($folder, self::FOLDERS)) {
			throw new \InvalidArgumentException('Invalid folder name');
		}

		if ($msg && (!ctype_alnum($msg) || strlen($msg) != 64)) {
			throw new \InvalidArgumentException('Invalid message ID');
		}

		$path = sprintf(USERS_STORAGE_PATH, $this->user->id);
		$path .= '/' . $folder;

		if ($msg) {
			$path .= '/' . $msg;
		}

		return $path;
	}

	/**
	 * Automatically delete messages after delay
	 */
	public function autoDelete(): void
	{
		foreach ($this->user->profile()->auto_delete as $folder => $delay) {
			if (!$delay) {
				continue;
			}

			$path = $this->path($folder);
			$dir = dir($path);

			while ($file = $dir->read()) {
				if ($file[0] === '.') {
					continue;
				}

				if (filemtime($path . '/' . $file) < time() - $delay) {
					@unlink($path);
				}
			}

			$dir->close();
		}
	}

	public function buildFromRaw(string $str): array
	{
		$msg = new Mail_Message;
		$msg->parse($str);

		$body = null;
		$encrypted = null;

		//FIXME: handle encryption correctly, see https://proton.me/support/pgp-mime-pgp-inline

		foreach ($msg->getParts() as $pid => $part) {
			if (strlen($part['content']) && false !== strpos($part['content'], '-----BEGIN PGP MESSAGE-----')) {
				$encrypted = $part['content'];
				continue;
			}

			// Always allow embedded text
			if (empty($part['name']) && empty($part['cid']) && strstr($part['type'], 'text/')) {
				continue;
			}

			$extension = null;
			$name = $part['name'] ?? null;
			$type = null;
			$size = strlen($part['content']);
			$deleted = false;

			if (($pos = strrpos($name, '.')) !== false) {
				$extension = strtolower(substr($name, $pos + 1));
			}

			if (MAX_ATTACHMENT_SIZE && $size > MAX_ATTACHMENT_SIZE) {
				$deleted = true;
			}
			elseif (!$extension || !array_key_exists($extension, ALLOWED_ATTACHMENT_TYPES)) {
				$deleted = true;
			}
			elseif (!empty($part['content']) && class_exists('\finfo')) {
				$finfo = new \finfo(\FILEINFO_MIME_TYPE);
				$type = $finfo->buffer($part['content']);
				unset($finfo);

				if (!in_array($type, ALLOWED_ATTACHMENT_TYPES)) {
					$deleted = true;
				}
			}

			$content = $deleted ? null : base64_encode($part['content']);
			$attachments[] = compact('name', 'type', 'extension', 'size', 'deleted', 'content');
		}

		if (!$encrypted_message) {
			$out['body'] = $msg->getBodyText(true);
		}

		$headers = strtok($str, "\r\n\r\n");
		strtok(false);
		unset($str);

		return [
			'headers'     => $headers,
			'from'        => $msg->getFrom(),
			'to'          => $msg->getTo(),
			'cc'          => $msg->getCc(),
			'date'        => $msg->getDate(),
			'message_id'  => $msg->getMessageId(),
			'in_reply_to' => $msg->getInReplyTo(),
			'attachments' => $attachments,
			'body'        => $body,
			'encrypted'   => $encrypted,
		];
	}

	public function receivePointer($pointer, bool $close = true): void
	{
		$msg = '';
		$size = 0;

		while (!feof($pointer)) {
			$data = fread($pointer, 8192);
			$size += strlen($data);

			if ($size > MAX_MESSAGE_SIZE) {
				if ($close) {
					fclose($pointer);
				}

				throw new \OutOfBoundsException('Received message is too large');
			}

			$msg .= $data;
		}

		if ($close) {
			fclose($pointer);
		}

		$this->receive($msg);
	}

	public function receive(string $str): void
	{
		if (strlen($str) > MAX_MESSAGE_SIZE) {
			throw new \OutOfBoundsException('Received message is too large');
		}

		$msg = $this->buildFromRaw($str);
		sodium_memzero($str);
		unset($str);

		$msg = json_encode($msg);
		$encrypted = sodium_crypto_box_seal($msg, $this->key);
		sodium_memzero($msg);
		unset($msg);

		$encrypted = sodium_bin2base64($encrypted, \SODIUM_BASE64_VARIANT_ORIGINAL);
		$this->store(self::QUEUE, $encrypted);
	}

	/**
	 * To protect a user against finding out when the message was stored (received)
	 * randomize its date/time.
	 *
	 * If the folder has auto-delete enabled, then the randomization is reduced.
	 * (it's a trade-off)
	 */
	public function getRandomTime(string $folder, int $time = null): ?int
	{
		$time ??= $time;
		$delay = $this->user->profile()->auto_delete[$folder] ?? null;

		if (!$delay) {
			return time() + ($sign * random_int(0, 3600*24*90));
		}

		$sign = random_int(0, 1) ? -1 : 1;

		if ($delay <= 3600*6) {
			// +/- 5 minutes
			$time += ($sign * random_int(0, 5*60));
		}
		elseif ($delay <= 3600*24*3) {
			// +/- 1 hour
			$time += ($sign * random_int(0, 3600));
		}
		elseif ($delay <= 3600*24*15) {
			// +/- 3 days
			$time += ($sign * random_int(0, 3600*24*3));
		}
		elseif ($delay <= 3600*24*90) {
			// +/- 10 days
			$time += ($sign * random_int(0, 3600*24*10));
		}
		else {
			// +/- 30 days
			$time += ($sign * random_int(0, 3600*24*30));
		}

		return $time;
	}

	public function store(string $folder, string $encrypted_message): string
	{
		$id = hash('sha256', $encrypted_message . random_bytes(10));
		$path = $this->path($folder, $id);

		$encrypted_message = gzencode($msg, 9);
		$size = strlen($encrypted_message);

		$quota = $this->user->getRemainingQuota($folder);

		if ($quota > -1 && $size > $quota) {
			throw new \OutOfBoundsException('Mailbox is full');
		}
		elseif ($size > MAX_MESSAGE_SIZE) {
			throw new \OutOfBoundsException('Message is too large');
		}

		@mkdir($path, 0777, true);
		file_put_contents($path, $encrypted_message);

		$mtime = $this->getRandomTime($folder);
		touch($path, $mtime);

		return $this->rehash($folder);
	}

	public function storePointer(string $folder, string $id, $pointer): string
	{
		$path = $this->path($folder, $id);
		$tmp_path = $this->path($folder) . '/.tmp-' . sha1(random_bytes(16) . $id);

		$fp = fopen($tmp_path, 'wb');
		$gzip = deflate_init(ZLIB_ENCODING_GZIP, ['level' => 9]);
		$size = 0;

		$quota = $this->user->getRemainingQuota($folder);

		while (!feof($pointer)) {
			$data = fread($fp, 8192);
			$data = deflate_add($gzip, $data, ZLIB_NO_FLUSH);
			$size += strlen($data);

			if ($quota > -1 && $size > $quota) {
				@unlink($tmp_path);
				throw new \OutOfBoundsException('Mailbox is full');
			}
			elseif ($size > MAX_MESSAGE_SIZE) {
				throw new \OutOfBoundsException('Message is too large');
			}

			fwrite($fp, $data);
		}

		fwrite($fp, deflate_add($gzip, '', ZLIB_FINISH));

		fclose($fp);
		fclose($gzip);

		rename($tmp_path, $path);

		$mtime = $this->getRandomTime($folder);
		touch($path, $mtime);

		return $this->rehash($folder);
	}

	public function list(string $folder): array
	{
		$path = $this->path($folder);
		$dir = dir($path);
		$list = [];

		while ($file = $dir->read()) {
			if (substr($file, 0, 1) === '.') {
				continue;
			}

			$list[] = $file;
		}

		$dir->close();

		return $list;
	}

	public function delete(string $folder, string $id): string
	{
		@unlink($this->path($folder, $id));
		return $this->rehash($folder);
	}

	public function fetch(string $folder, string $id): ?string
	{
		$path = $this->path($folder, $id);
		$message = @file_get_contents($path);

		if (empty($message)) {
			return null;
		}

		$message = gzdecode($message);
		return $message;
	}

	public function move(string $folder, string $id, string $destination_folder): array
	{
		$src = $this->path($folder, $id);
		$dst = $this->path($destination_folder, $id);

		rename($src, $dst);
		$this->rehash($folder);
		$this->rehash($destination_folder);

		return [
			$folder => $this->rehash($folder),
			$destination_folder => $this->rehash($destination_folder),
		];
	}

	protected function rehash(string $folder): string
	{
		$hash = sha1(random_bytes(16));
		file_put_contents($this->path($folder) . '/.hash2', $hash);
		rename($this->path($folder) . '/.hash2', $this->path($folder) . '/.hash');
		return $hash;
	}

	public function hash(string $folder): ?string
	{
		$path = $this->path($folder) . '/.hash';

		if (!file_exists($path)) {
			return null;
		}

		return file_get_contents($path);
	}
}
