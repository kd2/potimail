<?php

namespace Potimail;

use KD2\HTML\Markdown;
use KD2\HTML\Markdown_Extensions;

class Router_Exception extends \RuntimeException {}

class Router
{
	protected string $url;
	protected string $method;
	protected string $b;
	protected string $c;

	public function __construct(string $url)
	{
		$this->url = trim($url, '/');
		$this->method = $_SERVER['REQUEST_METHOD'];
	}

	public function processRequest()
	{
		try {
			$return = $this->route();
		}
		catch (Router_Exception $e) {
			http_response_code($e->getCode());
			echo json_encode($e->getMessage());
			return;
		}

		if (!http_response_code()) {
			http_response_code(200);
		}

		if ($return !== null) {
			header('Content-Type: application/json; charset=utf-8');
			echo json_encode($return, JSON_PRETTY_PRINT);
		}
	}

	public function param(string $key)
	{
		static $params = null;

		if (null === $params) {
			if (false !== strpos($_SERVER['CONTENT_TYPE'] ?? '', 'application/json')) {
				$params = json_decode(file_get_contents('php://input'), true);
				if (null === $params) {
					http_response_code(400);
					die("Invalid JSON value");
				}
			}
			else {
				$params =& $_POST;
			}
		}

		return $params[$key] ?? null;
	}

	public function params(...$names): array
	{
		$params = [];

		foreach ($names as $p) {
			$v = $this->param($p);

			if (null === $v || (is_string($v) && trim($v) === '')) {
				throw new Router_Exception(sprintf('Missing value for "%s"', $p), 400);
			}

			$params[$p] = $v;
		}

		return $params;
	}

	public function accept(...$methods): void
	{
		if (!in_array($this->method, $methods)) {
			throw new Router_Exception('Invalid method', 405);
		}
	}

	public function route()
	{
		$url = $this->url;
		$a = strtok($this->url, '/');
		$b = strtok('/');
		$c = strtok('/');
		$d = strtok(false);

		if ($a === 'e' && $b && !$c) {
			return $this->ephemeral($b);
		}
		elseif (($url === 'legal' || $url === '')) {
			return $this->markdown($a ?: 'home');
		}
		elseif ($a === 'auth' && $b && !$c) {
			return $this->auth($b);
		}
		elseif ($url === 'register') {
			return $this->register();
		}
		elseif ($url === 'receive') {
			return $this->receive();
		}
		elseif ($url === 'send' && $b == 'secure' && !$c) {
			return $this->secureSend();
		}
		elseif ($url === 'send' && $b == 'insecure' && !$c) {
			return $this->insecureSend();
		}
		elseif ($a === 'messages' && $b && $c && !$d) {
			return $this->messages($b, $c);
		}
		elseif ($a === 'directory' && $b && !$c) {
			return $this->directory($b);
		}
		elseif ($a === '.well-known' && $b === 'openpgpkey' && $c === 'hu' && $d) {
			return $this->wkd($d);
		}

		http_response_code(404);
		return null;
	}

	public function ephemeral(string $id, string $method): ?array
	{
		$this->accept('GET', 'POST');

		if ($method == 'GET') {
			$status = Ephemeral::status($id);

			if ($status === Ephemeral::BURN) {
				return ['burn' => true];
			}
			elseif ($status === Ephemeral::OK) {
				return Ephemeral::get($id);
			}
			else {
				return ['error' => 'Invalid address'];
			}
		}
		elseif ($method == 'POST') {
			if ($this->param('t') > time() - 600) {
				return Ephemeral::get($id);
			}
			else {
				return ['error' => 'Please retry'];
			}
		}

		return null;
	}

	public function markdown(string $page): void
	{
		$this->accept('GET');

		if (!ctype_alnum($page)) {
			http_response_code(404);
			return;
		}

		$path = ROOT_PATH . '/config/' . $page . '.md';

		if (!file_exists($path)) {
			http_response_code(404);
			return;
		}

		$cache_path = CACHE_PATH . '/' . $page . '.html';

		header('Content-Type: text/html; charset=utf-8');

		if (@filemtime($path) !== @filemtime($cache_path)) {
			$md = Markdown::instance();
			Markdown_Extensions::register($md);
			$html = $md->text(file_get_contents($path));

			@mkdir(dirname($cache_path), 0777, true);
			file_put_contents($cache_path, $html);
			touch($cache_path, filemtime($path));

			echo $html;
			return;
		}

		readfile($cache_path);
	}

	public function auth(string $step): array
	{
		$this->accept('POST');

		if ($step === 'init') {
			$r = UserSession::authInit(...$this->params('address'));
		}
		elseif ($step == 'verify') {
			$r = UserSession::authVerify(...$this->params('address', 'server_public_key', 'client_proof'));
		}
		else {
			throw new Router_Exception('Invalid URL', 403);
		}

		if (!$r) {
			throw new Router_Exception('Login failed.', 403);
		}

		return $r;
	}

	public function register(): array
	{
		$this->accept('POST');

		try {
			User::register(...$this->params('name', 'domain', 'public_key', 'pgp_public_key', 'pgp_private_key', 'keys'));
		}
		catch (\InvalidArgumentException $e) {
			throw new Router_Exception($e->getMessage(), 400);
		}

		return ['redirect_url' => '/m/'];
	}

	public function receive(): void
	{
		$this->accept('PUT');
		$this->requireSession();

		$m = new Mailbox($this->user);

		$msg = '';
		$size = 0;
		$fp = fopen('php://input', 'rb');

		while (!feof($fp)) {
			$msg .= fread($fp, 8192);
		}

		try {
			$m->receivePointer(fclose('php://input'));
		}
		catch (\OutOfBoundsException $e) {
			throw new Router_Exception($e->getMessage(), 413);
		}

		http_response_code(201);
	}

	public function send(): void
	{
		$this->accept('POST');
		$this->requireSession();

		$m = new Mailbox($this->user);

		if ($this->param('to') ?? null) {
			$m->sendToExternal($this->param('to'),
				$_POST['subject'] ?? null,
				$_POST['encrypted_message'] ?? null,
				$_POST['message'] ?? null
			);
		}
		else {
			$m->sendTo($_POST['hash'] ?? '', $_POST['server'] ?? '', $_POST['message'] ?? '');
		}

		http_response_code(200);
	}

	public function messages(string $folder, string $id)
	{
		$this->requireSession();

		if (empty($id)) {
			$this->accept('GET');
			return $m->list($folder);
		}
		elseif ($id === 'hash') {
			$this->accept('GET');
			return $m->hash($folder);
		}
		else {
			if ($this->method == 'GET') {
				$r =  $m->fetch($folder, $id);

				if (!$r) {
					throw new Router_Exception('Message not found', 404);
				}

				return $r;
			}
			elseif ($this->method == 'PUT') {
				$fp = fopen('php://input', 'rb');

				try {
					$r = $m->storePointer($folder, $id, $fp);
				}
				catch (\OutOfBoundsException $e) {
					throw new Router_Exception($e->getMessage(), 413);
				}
				finally {
					fclose($fp);
				}

				http_response_code(204);
				return $r;
			}
			elseif ($this->method == 'DELETE') {
				$m->delete($id);
				http_response_code(204);
			}
			elseif ($this->method == 'MOVE') {
				if (empty($_SERVER['HTTP_DESTINATION'])) {
					throw new Router_Exception('Missing destination header', 400);
				}

				if (!preg_match('!/messages/([a-z]+)/!', $_SERVER['HTTP_DESTINATION'], $match)) {
					throw new Router_Exception('Invalid destination', 400);
				}

				$r = $m->move($folder, $id, $match[1]);

				http_response_code(204);
				return $r;
			}
			else {
				$this->accept();
			}
		}

		return null;
	}

	public function wkd(string $hash): ?string
	{
		$key = Directory::getPublicKey($hash, $domain);

		if (!$key) {
			http_response_code(404);
			return null;
		}

		http_response_code(200);
		header('Content-Type: application/pgp-keys');
		header('Content-Disposition: inline; filename="pubkey.gpg"');

		return $key;
	}
}
