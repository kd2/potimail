<?php

class Server
{
	public function __construct(string $key)
	{
		$this->key = sodium_base642bin($key);
	}

	public function exists()
	{
		return file_exists($this->path);
	}

	public function register()
	{
		$hash = $_POST['hash'] ?? null;
		$message = $_POST['message'] ?? null;
	}

	public function receive()
	{
		$hash = $_POST['hash'] ?? null;
		$message = $_POST['message'] ?? null;
	}

	public function route()
	{
		$ciphertext = $_POST['data'] ?? null;
		$nonce = $_POST['nonce'] ?? null;
		$signature = $_POST['signature'] ?? null;

		if (!$this->exists()) {
			throw new \LogicException('Unknown server key', 404);
		}

		if (!sodium_crypto_sign_verify_detached($signature, $nonce . $ciphertext, $this->key)) {
			throw new \LogicException('Invalid signature', 400);
		}

		$secret_key = $this->getSelfSecretKey();

		$data = sodium_crypto_secretbox_open($ciphertext, $nonce, $secret_key);
		sodium_memzero($ciphertext);
		sodium_memzero($nonce);
		sodium_memzero($secret_key);

		if (!$data) {
			throw new \LogicException('Invalid encrypted data', 400);
		}

		$data = json_decode($data);

		// We need to forward this to the next node
		if (isset($data->next_hop, $data->data, $data->nonce)) {
			$message = [
				'data'      => $data->data,
				'nonce'     => $data->nonce,
				'signature' => sodium_crypto_sign_detached($data->data . $data->nonce, $this->getSelfSecretKey()),
			];

			// Forward to next node
			$url = 'https://' . $this->fqdn . '/network/route';
			(new HTTP)->POST($url, $message);
		}
		elseif (isset($data->hash, $data->message)) {
			$user = new User($data->hash);

			if (!$user->exists()) {
				return;
			}

			$box = new Mailbox($user);
			$box->store(Mailbox::QUEUE, $data->message);
		}
		else {
			throw new \LogicException('Invalid encrypted data', 400);
		}
	}
}
