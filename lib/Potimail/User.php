<?php

namespace Potimail;

class User
{
	protected string $path;
	protected string $hash;
	protected UserProfile $profile;

	const PATH = STORAGE_PATH . '/users/%.2s/%1$s';

	static public function getHash(string $name, string $domain): string
	{
		// we need to hash the name, so that we can use WKD directly
		return hash('sha256', sha1($name) . '@' . $domain);
	}

	static public function getHashFromWKD(string $hash, string $domain): string
	{
		if (!ctype_alnum($hash) || strlen($hash) !== 40) {
			throw new \RuntimeException('Invalid hash: ' . $hash);
		}


		return hash('sha256', $hash . '@' . $domain);
	}

	static public function path(string $hash): string
	{
		return sprintf(self::PATH, $hash);
	}

	public function __get(string $key)
	{
		if ($key == 'path' || $key == 'hash') {
			return $this->$key;
		}

		throw new \RuntimeException('Property does not exist: ' . $key);
	}

	public function __construct(string $hash)
	{
		$this->path = self::path($hash);
		$this->hash = $hash;
	}

	public function profile(): UserProfile
	{
		if (!isset($this->profile)) {
			$this->profile = new UserProfile($this);
		}

		return $this->profile;
	}

	public function delete(): void
	{
		$this->profile->shred();
		$this->recursiveDelete($this->path);
		@rmdir($this->path);
		UserSession::logout();
	}

	public function updateKeys(string $keys): void
	{
		$profile = $this->profile();

		foreach ($keys as $key => $value) {
			if (!isset($profile::KEYS[$key]) || trim($value) === '') {
				throw new \InvalidArgumentException('Unknown or empty key: ' . $key);
			}

			$profile->$key = trim($value);
		}

		$profile->save();
	}

	static public function validateName(string $name): bool
	{
		return (bool) preg_match('/^[a-z]+[a-z0-9]*(?:[\._-][a-z0-9]+)*$/', $name);
	}

	public function exists(): bool
	{
		return file_exists($this->path . '/user.json');
	}

	public function getRemainingQuota(string $folder): int
	{
		$profile = $this->profile();
		$quota = $profile->quota[$folder] ?? $profile->quota[Mailbox::INBOX];

		if ($quota == -1) {
			return -1;
		}
	}

	static public function isUsingTor(): bool
	{
		$ip = $_SERVER['REMOTE_ADDR'] ?? null;
		$cache_file = CACHE_DIR . '/tor_nodes.php';

		if (!file_exists($cache_file) || filemtime($cache_file) < time() - 3600*24) {
			$list = file(TOR_EXIT_NODES_LIST_URL);
			$list = array_map('trim', $list);
			$list = array_filter($list);
			$tor_nodes = $list;
			$out = "<?php\n" . var_export($list, true);
			file_put_contents($cache_file, $out);
		}
		else {
			$tor_nodes = include($cache_file);
		}

		return in_array($ip, $tor_nodes, true);
	}

	static public function get(string $address): ?self
	{
		$address = trim($address);
		$address = strtolower($address);

		if ($address === '') {
			return null;
		}

		if (false === strpos($address, '@')) {
			return null;
		}

		$name = strtok($address, '@');
		$domain = strtok(false);

		if (!self::validateName($name)) {
			return null;
		}

		if (!in_array($domain, DOMAINS, true)) {
			return null;
		}

		$user = new self(self::getHash($name, $domain));

		if (!$user->exists()) {
			return null;
		}

		return $user;
	}

	static public function register(string $name, string $domain, string $public_key, string $pgp_public_key, string $pgp_private_key,  array $keys): void
	{
		$name = strtolower($name);
		$domain = strtolower($domain);

		if (!self::validateName($name)) {
			throw new \InvalidArgumentException('Invalid username');
		}

		if (!in_array($domain, DOMAINS, true)) {
			throw new \InvalidArgumentException('Invalid domain');
		}

		if (count($keys) !== 3) {
			throw new \InvalidArgumentException('Exactly 3 keys are required');
		}

		if (strlen(sodium_hex2bin($public_key)) != SODIUM_CRYPTO_BOX_PUBLICKEYBYTES) {
			throw new \InvalidArgumentException('Wrong public key length');
		}

		if (strlen($pgp_public_key) < 128) {
			throw new \InvalidArgumentException('Wrong PGP public key length');
		}

		if (strlen($pgp_private_key) < 128) {
			throw new \InvalidArgumentException('Wrong PGP public key length');
		}

		$user = new self(self::getHash($name, $domain));

		if ($user->exists()) {
			throw new \InvalidArgumentException('This username already exists');
		}

		$user->profile = new UserProfile($user);

		$required = ['identifier', 'salt'];
		$pkeys = [];

		foreach ($keys as $i => $key) {
			$pkey = [];

			foreach ($required as $name) {
				if (!isset($key[$name]) || strlen($key[$name]) < 10) {
					throw new \InvalidArgumentException(sprintf('Invalid key #%d: missing "%s"', $i, $name));
				}

				$pkey[$name] = $key[$name];
			}

			$pkey['private'] = $key['private'] ?? null;
			$pkeys[] = $pkey;
		}

		shuffle($pkeys);
		$user->profile->keys = $pkeys;

		$user->profile->public_key = $public_key;
		$user->profile->pgp_public_key = self::cleanArmoredPGPKey($pgp_public_key);
		$user->profile->pgp_private_key = $pgp_private_key;

		$user->profile->save();
	}

	static public function cleanArmoredPGPKey(string $str): string
	{
		$str = preg_replace('/-----(?:END|BEGIN) PGP[^-]+-----/', '', $str);
		return str_replace("\n", '', trim($str));

	}
}
