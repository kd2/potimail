<?php

namespace Potimail;

class UserProfile
{
	private User $user;
	private string $path;

	/**
	 * Users login keys for SRP login, each derived from a different password
	 * with libsodium by the client, and each containing:
	 * - 'identifier': the public key generated from the password seed
	 * - 'salt': a random salt
	 * - 'private': the users private key, encrypted using the password private key
	 *
	 * @see https://eric.mann.blog/a-libsodium-based-secure-remote-password-scheme-for-php/
	 * @see https://proton.me/blog/data-recovery-end-to-end-encryption
	 */
	private array $keys = [];

	/**
	 * Users public key
	 */
	private string $public_key;

	/**
	 * PGP Public key (cleartext)
	 */
	private string $pgp_public_key;

	/**
	 * PGP private key, encrypted with users private key
	 */
	private string $pgp_private_key;

	/**
	 * List of folders, and corresponding delay (in seconds) after which the messages
	 * in that folder are automatically deleted by the server
	 */
	private array $auto_delete;

	private int $max_message_size;
	private int $max_attachment_size;
	private int $max_sent_messages_24h;

	/**
	 * List of available quota, excepted queue
	 * @var array
	 */
	private int $quota;
	private int $queue_quota;

	static private array $defaults;

	static public function setDefaults(array $d)
	{
		self::$defaults = $d;
	}

	public function __toString(): string
	{
		$a = get_object_vars($this);
		unset($a['user'], $a['path']);
		return json_encode($a, JSON_PRETTY_PRINT);
	}

	public function __construct(User $user)
	{
		$this->user = $user;
		$this->path = $user->path . '/user.json';

		if (file_exists($this->path)) {
			$this->import(file_get_contents($this->path));
		}
	}

	public function __set(string $key, $value)
	{
		if (!property_exists($this, $key)) {
			throw new \RuntimeException('Property does not exist: ' . $key);
		}

		if ($key == 'user' || $key == 'path') {
			throw new \RuntimeException('Property cannot be set here: ' . $key);
		}

		$this->$key = $value;
	}

	public function __get(string $key)
	{
		if (!property_exists($this, $key)) {
			throw new \RuntimeException('Property does not exist: ' . $key);
		}

		if (!isset($this->$key)) {
			return null;
		}

		return $this->$key;
	}

	public function import(string $json) {
		$json = json_decode($json);

		if (!$json) {
			throw new \InvalidArgumentException('Invalid JSON');
		}

		foreach ($json as $key => $value) {
			$this->$key = $value;
		}
	}

	public function save(): void
	{
		$path = $this->path;

		if (file_exists($path)) {
			// First shred the entire profile
			$this->shred();
		}

		@mkdir(dirname($path), 0777, true);
		file_put_contents($path, (string) $this);

		// Make sure we don't leave a trace of when the file was modified
		touch($path, 0);
	}

	public function shred(): void
	{
		// Overwrite file 3 times
		for ($i = 0; $i < 3; $i++) {
			$this->overwrite($this->path);
		}
	}

	protected function overwrite(string $file): void
	{
		$fp = fopen($file, 'r+');
		fseek($fp, 0, SEEK_END);
		$size = ftell($fp);
		fseek($fp, 0, SEEK_SET);

		while (!feof($fp) && $i < $size) {
			$block_size = ($size - $i) < 8192 ? ($size - $i) : 8192;
			fwrite($fp, random_bytes($block_size));
			$i += $block_size;
		}

		fclose($fp);
	}
}
