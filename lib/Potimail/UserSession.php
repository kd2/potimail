<?php

namespace Potimail;

use stdClass;

/**
 * User session management, using SRP with libsodium
 *
 * On SRP, see also:
 * @see https://github.com/artisansdk/srp/
 * @see https://en.wikipedia.org/wiki/Secure_Remote_Password_protocol
 * @see https://bitbucket.org/simon_massey/thinbus-srp-js/src/master/
 * @see https://eric.mann.blog/a-libsodium-based-secure-remote-password-scheme-for-php/
 * @see https://code.falk-m.de/srp/
 * @see https://github.com/dalabarge/srp-demo
 * @see https://www.jsdelivr.com/package/npm/@wault-pw/srp6a-webcrypto
 * @see https://github.com/ProtonMail/pm-srp
 * @see https://github.com/ProtonMail/go-srp
 */
class UserSession
{
	static public function garbageCollect(): void
	{
		foreach (glob(STORAGE_PATH . '/sessions/*/*') as $file) {
			if (basename($file)[0] === '.') {
				continue;
			}

			if (filemtime($file) < time()) {
				@unlink($file);
			}
		}
	}

	static public function getCurrentSessionCookie(): ?string
	{
		$cookie = $_COOKIE['s'] ?? null;

		if (!$cookie) {
			return null;
		}

		if (substr_count($cookie, ':') !== 1) {
			return null;
		}

		// Cookie ID should be HASH:HASH
		if (strlen($cookie) != 40+1+40) {
			return null;
		}

		if (!ctype_alnum(str_replace(':', '', $cookie))) {
			return null;
		}

		return $cookie;
	}

	static public function getLoggedUser(): ?self
	{
		$cookie = self::getCurrentSessionCookie();

		if (!$cookie) {
			return null;
		}

		$user_hash = strtok($cookie, ':');
		$user_session_id = strtok(false);

		if (empty($user_hash) || empty($user_session_id) || !ctype_alnum($user_hash) || strlen($user_hash) !== 40) {
			return null;
		}

		$user = new User($user_hash);
		$session = self::get($user_session_id);

		if (!$session) {
			return null;
		}

		self::extend($user, $user_session_id);

		return $user;
	}

	static protected function getPath(User $user, string $id)
	{
		if (!ctype_alnum($id) || strlen($id) !== 40) {
			throw new \InvalidArgumentException('Invalid session ID: ' . $id);
		}

		return sprintf('%s/_sessions/%s', $user->path, $id);
	}

	static public function store(User $user, string $id, array $data, int $expiry = 3600)
	{
		$path = self::getPath($user, $id);

		@mkdir(dirname($path), 0777, true);

		file_put_contents($path, json_encode($data));
		touch($path, time() + $expiry);
	}

	static public function get(User $user, string $id): ?stdClass
	{
		$path = self::getPath($user, $id);

		// Keys file does not exist: step1 wasn't done
		if (!file_exists($path)) {
			return null;
		}

		// There shouldn't be more than 5 minutes between step 1 and 2
		if (@filemtime($path) < time()) {
			@unlink($path);
			return null;
		}

		return json_decode(@file_get_contents($path));
	}

	static public function delete(User $user, string $id): void
	{
		$path = self::getPath($user, $id);

		@unlink($path);
	}

	static public function extend(User $user, string $id, int $expiry = 3600): void
	{
		@touch(self::getPath($user, $id), time() + $expiry);
	}

	/**
	 * Login step 1 for SRP
	 * @see https://user-images.githubusercontent.com/53541863/111525037-c1809780-8722-11eb-8111-db700a05f1c1.png
	 * @see https://eric.mann.blog/a-libsodium-based-secure-remote-password-scheme-for-php/
	 */
	static public function authInit(string $address): ?array
	{
		// Sometimes, remove expired sessions
		if (time() % 10 == 0) {
			self::garbageCollect();
		}

		$user = User::get($address);

		// Check that user is valid
		if (!$user) {
			return null;
		}

		$out = [];

		foreach ($user->profile()->keys as $user_key) {
			$profile = $user->profile();

			// Create an ephemeral keypair
			$keypair = sodium_crypto_kx_keypair();
			$public_key = sodium_crypto_kx_publickey($keypair);

			self::store($user, sha1($public_key), [
				'temp_keypair'    => sodium_bin2hex($keypair),
				'identifier'      => $user_key->identifier,
			], 600);

			// Derive the shared secret
			$session_keys = sodium_crypto_kx_server_session_keys($keypair, sodium_hex2bin($user_key->identifier));
			$client_secret = $session_keys[1];
			$server_secret = $session_keys[0];

			// Generate the server's proof
			$server_message = random_bytes(32);
			$server_hash = sodium_crypto_generichash($server_message, $server_secret);
			$proof = sodium_bin2hex($server_message . $server_hash);

			$out[] = [
				'salt'              => $user_key->salt,
				'server_public_key' => sodium_bin2hex($public_key),
				'server_proof'      => $proof,
			];

			sodium_memzero($keypair);
		}

		return $out;
	}

	/**
	 * Login step 2 for SRP
	 */
	static public function authVerify(string $address, string $server_public_key, string $client_proof): ?array
	{
		$user = User::get($address);

		// Check that user exists
		if (!$user) {
			return null;
		}

		$session_id = sha1(sodium_hex2bin($server_public_key));
		$session = self::get($user, $session_id);

		// Remove session after fetch
		#self::delete($user, $session_id);

		if (!$session || !isset($session->temp_keypair)) {
			return null;
		}

		$profile = $user->profile();
		$user_key = null;

		foreach ($profile->keys as $key) {
			if ($key->identifier == $session->identifier) {
				$user_key = $key;
				break;
			}
		}

		if (!$user_key) {
			return null;
		}

		$client_public_key = sodium_hex2bin($session->identifier);
		$keypair = sodium_hex2bin($session->temp_keypair);

		// Derive the shared secret
		$session_keys = sodium_crypto_kx_server_session_keys($keypair, $client_public_key);
		$client_secret = $session_keys[0];
		$server_secret = $session_keys[1];

		// Validate the client's proof
		$client_proof = sodium_hex2bin($client_proof);
		$message = substr($client_proof, 0, 32);
		$hash = substr($client_proof, 32);

		$verify = sodium_crypto_generichash($message, $client_secret);

		// If the hash doesn't match, err!
		if (!hash_equals($verify, $hash)) {
			http_response_code(500);
			var_dump('message2: ' . sodium_bin2hex($message), sodium_bin2hex($hash), sodium_bin2hex($verify));
			return null;
		}

		// Create session
		$new_session_id = sha1(random_bytes(16));
		self::sessionStore($user, $new_session_id, ['user' => $user->hash]);

		$path = self::sessionFilePath($r->session);
		@mkdir(dirname($path), 0777, true);
		file_put_contents($path, $user->hash);

		setcookie('s', $new_session_id, [
			'expires'  => 0,
			'path'     => '/',
			'secure'   => IS_HTTPS,
			'httponly' => true,
			'samesite' => 'Strict',
		]);

		return [
			'session_id'      => $new_session_id,
			'public_key'      => $profile->public_key,
			'private_key'     => $user_key->private,
			'pgp_public_key'  => $profile->pgp_public_key,
			'pgp_private_key' => $profile->pgp_private_key,
		];
	}

	static public function logout(): void
	{
		if (!isset($_COOKIE['s'])) {
			return;
		}

		setcookie('s', null, [
			'expires'  => -1,
			'path'     => '/',
			'secure'   => IS_HTTPS,
			'httponly' => true,
			'samesite' => 'Strict',
		]);

		unset($_COOKIE['s']);
	}
}
