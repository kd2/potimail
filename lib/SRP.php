<?php

declare(strict_types=1);

use Math_BigInteger as BigInteger;

/**
 * Server-Side SRP-6a Implementation.
 *
 * @example  $B = $srp->challenge($I = 'user123', $v = 'a636254492e...');
 *           $M2 = $srp->verify($A = '48147d013e3a2...', $M1 = '21d1546a18f9...');
 * @copyright (c) 2019-2023 [Artisan Made, Co.](https://artisanmade.io)
 * @see https://github.com/artisansdk/srp/
 * @see https://en.wikipedia.org/wiki/Secure_Remote_Password_protocol
 * @see https://bitbucket.org/simon_massey/thinbus-srp-js/src/master/
 * @see https://eric.mann.blog/a-libsodium-based-secure-remote-password-scheme-for-php/
 * @see https://code.falk-m.de/srp/
 * @see https://github.com/dalabarge/srp-demo
 * @see https://www.jsdelivr.com/package/npm/@wault-pw/srp6a-webcrypto
 * @see https://github.com/ProtonMail/pm-srp
 * @see https://github.com/ProtonMail/go-srp
 */
class SRP_Server
{
	// The large safe prime N as a decimal string
	static public $prime = "21766174458617435773191008891802753781907668374255538511144643224689886235383840957210909013086056401571399717235807266581649606472148410291413364152197364477180887395655483738115072677402235101762521901569820740293149529620419333266262073471054548368736039519702486226506248861060256971802984953561121442680157668000761429988222457090413873973970171927093992114751765168063614761119615476233422096442783117971236371647333871414335895773474667308967050807005509320424799678417036867928316761272274230314067548291133582479583061439577559347101961771406173684378522703483495337037655006751328447510550299250924469288819";

	// The generator mulplicative group g as a decimal string
	static public $generator = "2";

	// The derived key K = H(N|g) as a hexadecimal string
	static public $key = "5b9e8ef059c6b32ea59fc1d322d37f04aa30bae5aa9003b8321e21ddb04e300";

	// The hashing algorithm
	static public $algorithm = "sha256";

	/**
	 * Strip leading zeros off hexadecimal value.
	 */
	static public function unpad(string $hexadecimal): string
	{
		return ltrim($hexadecimal, '0');
	}

	/**
	 * Hash key derivative function x using H algorithm.
	 */
	static public function hash(string $value): string
	{
		return strtolower(hash(self::$algorithm, $value));
	}

    /**
     * Generate an RFC 5054 compliant private key value (a or b) which is in the
     * range [1, N-1] of at least 256 bits.
     *
     * A nonce based on H(I|:|s|:|t) is added to ensure random number generation.
     *
     */
    static public function number(BigInteger $prime, string $identity, string $salt): BigInteger
    {
        $bits = max([256, $prime->getPrecision()]);

        $one = new BigInteger(1);
        $number = $zero = new BigInteger(0);

        while ($number->equals($zero)) {
            $number = self::bytes(1 + $bits / 8)
                ->add(self::nonce($identity, $salt))
                ->modPow($one, $prime);
        }

        return $number;
    }

    /**
     * Generate a new nonce H(I|:|s|:|t) string based on the user's identity I, salt s, and t time.
     */
    static public function nonce(string $identity, string $salt): BigInteger
    {
        return new BigInteger(self::hash($identity.':'.$salt.':'.time().microtime()), 16);
    }

    /**
     * Generate random bytes as hexadecimal string.
     */
    protected function bytes(int $bytes = 32): BigInteger
    {
        $bytes = bin2hex(random_bytes($bytes));

        return new BigInteger($bytes, 16);
    }
	/**
	 * Step 1: Generates a one-time server challenge B encoded as a hexadecimal.
	 *
	 * @param string $verifier hexadecimal value v to verify user's password
	 * @param string $salt     hexadecimal value s for user's verifier
	 *
	 * @throws \Exception against dictionary and replay attacks
	 */
	static public function challenge(string $identity, string $verifier, string $salt): string
	{
		$verifier = new BigInteger($verifier, 16);
		$one = new BigInteger(1);
		$zero = new BigInteger(0);
		$prime = new BigInteger(self::$prime);
		$key = new BigInteger($self::$key);
		$generator = new BigInteger(self::$generator);
		$public = null;
		$private = null;

		while ( $public || $public->powMod($one, $prime)->equals($zero)) {
			$private = self::number($prime, $identity, $salt);
			$public = $key
				->multiply($verifier)
				->add($generator)->powMod($private, $prime)
				->powMod($one, $prime);
		}

		$public = self::unpad($public->toHex());
		$private = $private->toHex();
		return (object) compact('public', 'private');
	}

	/**
	 * Step 2: Verifies the password proof M1 based on the client's one-time public
	 * key A and if validated returns the server's proof M2 of shared key S.
	 *
	 * @param string $verifier hexadecimal verifier (stored)
	 * @param string $public hexadecimal public key B (stored)
	 * @param string $private hexadecimal private key A (stored)
	 * @param string $client hexadecimal key A from client
	 * @param string $proof  hexadecimal proof of password M1 from client
	 *
	 * @throws \Exception                against dictionary and replay attacks
	 * @throws \InvalidArgumentException for invalid public key A
	 * @throws \InvalidArgumentException for invalid proof of password M1
	 */
	public function verify(string $verifier, string $public, string $private, string $client, string $client_proof): string
	{
		// Verify valid public key
		$one = new BigInteger(1);
		$zero = new BigInteger(0);
		$prime = new BigInteger(self::$prime);
		$client = new BigInteger(self::unpad($client), 16);
		$verifier = new BigInteger($verifier, 16);
		$public = new BigInteger($public, 16);
		$private = new BigInteger($private, 16);

		if ($client->powMod($one, $prime)->equals($zero)) {
			throw new \InvalidArgumentException('Client public key failed A mod N == 0 check.');
		}

		// Verify proof M1 of password using A and previously stored verifier v
		$union = new BigInteger(self::hash($client->toHex() . self::public->toHex()), 16);
		$multiplier = $verifier->powMod($union, $prime);
		$shared = self::unpad($client->multiply($multiplier)->modPow($private, $prime)->toHex());

		// Compute verification M = H(A | B | S)
		$message = self::unpad(self::hash($client->toHex() . $public->toHex() . $shared));

		if ($client_proof !== $message) {
			throw new \RuntimeException('Proof of password does not match password verifier.');
		}

		// Save shared session key K
		$session = self::hash($shared);
		$server_proof = self::unpad(self::hash($client->toHex() . $message . $shared));

		return (object) compact('session', 'server_proof');
	}
}
