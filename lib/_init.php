<?php

namespace Potimail;

use KD2\ErrorManager;

\spl_autoload_register(function (string $classname): void {
	$classname = ltrim($classname, '\\');

	// PSR-0 autoload
	$filename = str_replace('\\', '/', $classname);
	$path = __DIR__ . '/' . $filename . '.php';

	if (file_exists($path)) {
		require_once $path;
	}
}, true);

$sodium_path = __DIR__ . '/../vendor/paragonie/sodium_compat/autoload.php';

// Fallback for libsodium
if (!extension_loaded('sodium')) {
	if (file_exists($sodium_path)) {
		require_once $sodium_path;
	}
	else {
		die("Error: Potimail requires libsodium, and it's not installed");
	}
}

require_once __DIR__ . '/../config/config.php';

ErrorManager::enable(ERRORS_SHOW ? ErrorManager::DEVELOPMENT : ErrorManager::PRODUCTION);
ErrorManager::setLogFile(ERRORS_LOG);

if (ERRORS_EMAIL) {
	ErrorManager::setEmail(ERRORS_EMAIL);
}

const ROOT_PATH = __DIR__ . '/..';

if (!file_exists(ROOT_PATH . '/config/config.php')) {
	die("Error: Potimail is not configured, file config/config.php is missing.");
}

const CACHE_PATH = STORAGE_PATH . '/cache';
