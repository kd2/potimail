<?php

namespace Potimail;

define(__NAMESPACE__ . '\IS_HTTPS', !empty($_SERVER['HTTPS']));

require __DIR__ . '/../lib/_init.php';

$r = new Router($_SERVER['REQUEST_URI']);
$r->processRequest();